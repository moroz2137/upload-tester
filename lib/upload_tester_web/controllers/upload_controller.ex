defmodule UploadTesterWeb.UploadController do
  use UploadTesterWeb, :controller

  def create(conn, %{"file" => %Plug.Upload{path: file_path}}) do
    contents = File.read!(file_path)
    json(conn, %{file: contents})
  end
end
