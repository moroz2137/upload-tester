defmodule UploadTesterWeb.PageController do
  use UploadTesterWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
