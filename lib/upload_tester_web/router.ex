defmodule UploadTesterWeb.Router do
  use UploadTesterWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", UploadTesterWeb do
    pipe_through :browser

    get "/", PageController, :index
  end

  scope "/api", UploadTesterWeb do
    pipe_through :api

    post "/upload", UploadController, :create
  end

  # Other scopes may use custom stacks.
  # scope "/api", UploadTesterWeb do
  #   pipe_through :api
  # end
end
